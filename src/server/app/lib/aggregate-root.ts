import { AggregateRoot as CqrsAggregateRoot } from '@nestjs/cqrs';

export class AggregateRoot extends CqrsAggregateRoot {
  version: number;

  constructor() {
    super();
    this.version = 0;
  }

  apply(event, isFromHistory = false) {
    if (!isFromHistory) {
      this.version += 1;
    }

    super.apply(event, isFromHistory);
  }
}
