import { Injectable } from "@nestjs/common";
import { ofType, Saga } from "@nestjs/cqrs";
import { map, Observable } from "rxjs";
import { SendEmail } from "./communications/commands/send-email.command";
import { ProfileCreated } from "./profile-management/profile/events/profile-created.event";

@Injectable()
export class AppSagas {
  @Saga()
  onProfileCreated(events$: Observable<any>) {
    return events$.pipe(
      ofType(ProfileCreated),
      map((event) => new SendEmail(event.profile.profileId)),
    );
  }
}
