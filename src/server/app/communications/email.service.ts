import { Injectable, Logger } from "@nestjs/common";
import { createTransport, Transporter } from 'nodemailer';
import { MailOptions } from "nodemailer/lib/smtp-transport";

@Injectable()
export class EmailService {
  private readonly logger = new Logger(EmailService.name);

  private transporter: Transporter<any>;

  constructor() {
    this.transporter = createTransport({
      host: 'localhost',
      port: 1025,
      secure: false,
    });
  }

  async send(options: MailOptions) {
    let info = await this.transporter.sendMail(options);

    this.logger.debug('email sent', { messageId: info.messageId });
  }
}
