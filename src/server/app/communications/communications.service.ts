import { Injectable } from "@nestjs/common";
import { CommandBus } from "@nestjs/cqrs";
import { SendEmail } from "./commands/send-email.command";

@Injectable()
export class CommunicationsService {
  constructor(private readonly commandBus: CommandBus) {}

  async sendWelcomeEmail(to: string) {
    return this.commandBus.execute(new SendEmail(to));
  }
}
