import { Module } from "@nestjs/common";
import { CqrsModule } from "@nestjs/cqrs";
import { SendEmailHandler } from "./commands/send-email.command";
import { EmailService } from "./email.service";

const commandHandlers = [
  SendEmailHandler,
];

@Module({
  imports: [CqrsModule],
  providers: [EmailService, ...commandHandlers],
})
export class CommunicationsModule {}
