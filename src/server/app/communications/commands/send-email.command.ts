import { CommandHandler, ICommand, ICommandHandler } from "@nestjs/cqrs";
import * as ejs from 'ejs';
import { EmailService } from "../email.service";

export class SendEmail implements ICommand {
  constructor(public readonly to: string) {}
}

@CommandHandler(SendEmail)
export class SendEmailHandler implements ICommandHandler<SendEmail> {
  constructor(private readonly emailService: EmailService) {}

  async execute(command: SendEmail): Promise<any> {
    const html = await ejs.renderFile('/home/emrys/Development/pet-project/src/server/app/communications/emails/welcome.ejs', {
      user_firstname: 'Muddy Waters',
      confirm_link: 'http://www.pet-project.in/confirm=mwaters@pm.me',
    });

    return this.emailService.send({
      to: 'Muddy Waters <mwaters@pme.me>',
      html,
    });
  }
}
