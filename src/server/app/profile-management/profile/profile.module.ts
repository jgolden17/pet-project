import { Module } from "@nestjs/common";
import { CqrsModule } from "@nestjs/cqrs";
import { MongooseModule } from "@nestjs/mongoose";
import { DatabaseModule } from "../../database/database.module";
import { CreateProfileHandler } from "./commands/create-profile.command";
import { ProfileCreatedHandler } from "./events/profile-created.event";
import { ProfileController } from "./profile.controller";
import { providers } from "./profile.providers";
import { ProfileRepository } from "./profile.repository";
import { ProfileService } from "./profile.service";
import { GetProfileByIdHandler } from "./queries/get-profile-by-id.query";
import { Profile, ProfileSchema } from "./schemas/profile.schema";

const commandHandlers = [
  CreateProfileHandler,
];

const eventHandlers = [
  ProfileCreatedHandler,
];

const queryHandlers = [
  GetProfileByIdHandler,
];

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([{ name: Profile.name, schema: ProfileSchema }]),
    DatabaseModule,
  ],
  controllers: [ProfileController],
  providers: [
    ProfileRepository,
    ProfileService,
    ...commandHandlers,
    ...eventHandlers,
    ...queryHandlers,
    ...providers,
  ],
})
export class ProfileModule {}
