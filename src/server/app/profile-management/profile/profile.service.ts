import { Injectable } from "@nestjs/common";
import { CommandBus, QueryBus } from "@nestjs/cqrs";
import { ID } from "src/shared/core.types";
import { IProfile } from "src/shared/profile.types";
import { CreateProfile } from "./commands/create-profile.command";
import { GetProfileById } from "./queries/get-profile-by-id.query";

@Injectable()
export class ProfileService {
  constructor(private readonly commandBus: CommandBus, private readonly queryBus: QueryBus) {}

  async createProfile(profile: IProfile) {
    const result = await this.commandBus.execute(new CreateProfile(profile));

    console.log('results', result);

    return result;
  }

  async findById(id: ID) {
    return this.queryBus.execute(new GetProfileById(id));
  }
}
