import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ID } from 'src/shared/core.types';
import { ProfileDTO } from 'src/shared/profile.types';
import { ProfileService } from './profile.service';

type Response = {
  message: string;
  expectedVersion: number;
}

@Controller('profiles')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Post()
  async create(@Body() createProfileDto: ProfileDTO): Promise<Response> {
    const expectedVersion = await this.profileService.createProfile(createProfileDto);

    return {
      message: 'Profile created',
      expectedVersion,
    };
  }

  @Get(':id')
  getProfileById(@Param('id') id: ID) {
    return this.profileService.findById(id);
  }
}
