import { Inject, Injectable } from '@nestjs/common';
import { IProfile } from 'src/shared/profile.types';
import { Model } from 'mongoose';
import { ObjectId } from 'mongodb';
import { ID } from 'src/shared/core.types';

@Injectable()
export class ProfileRepository {
  constructor(@Inject('PROFILE_MODEL') private readonly profileModel: Model<IProfile>) {}

  createId() {
    return new ObjectId().toHexString();
  }

  async findById(id: ID) {
    return this.profileModel.findById({ id });
  }

  async persist(projection: IProfile & { id: ID }) {
    await this.profileModel.create({
      _id: projection.id,
      profileId: projection.profileId,
      slug: projection.slug,
      name: projection.name,
      species: projection.species,
    });
  }
}
