import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { ID } from "src/shared/core.types";
import { SpeciesEnum } from "src/shared/profile.types";

@Schema()
export class Profile {
  @Prop({ require: true })
  id: ID;

  @Prop({ required: true, index: true })
  profileId: string;

  @Prop({ required: true })
  slug: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  species: SpeciesEnum;
}

export type ProfileDocument = Profile & Document;

export const ProfileSchema = SchemaFactory.createForClass(Profile);
