import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ID } from 'src/shared/core.types';
import { ProfileRepository } from '../profile.repository';

export class GetProfileById {
  constructor(public readonly id: ID) {}
}

@QueryHandler(GetProfileById)
export class GetProfileByIdHandler implements IQueryHandler<GetProfileById> {
  constructor(private readonly repository: ProfileRepository) {}

  async execute(query: GetProfileById): Promise<any> {
    return this.repository.findById(query.id);
  }
}
