import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { IProfileInput } from 'src/shared/profile.types';
import { Profile } from '../domain/profile.aggregate';
import { ProfileRepository } from '../profile.repository';
import { ProfileSlug } from '../domain/profile-slug.value';
import { ProfileId } from '../domain/profile-id.value';

export class CreateProfile {
  constructor(public readonly profile: IProfileInput) {}
}

@CommandHandler(CreateProfile)
export class CreateProfileHandler implements ICommandHandler<CreateProfile> {
  constructor(
    private readonly repository: ProfileRepository,
    private publisher: EventPublisher,
  ) {}

  private getSlug(command: CreateProfile) {
    return ProfileSlug.create(command.profile.profileId);
  }

  private getProfileId(command: CreateProfile) {
    return ProfileId.create(command.profile.profileId);
  }

  private getProfile() {
    return this.publisher.mergeObjectContext(
      new Profile(this.repository.createId()),
    );
  }

  async execute(command: CreateProfile): Promise<any> {
    const slug = this.getSlug(command);

    const profileId = this.getProfileId(command);

    const profile = this.getProfile();

    profile.create({
      ...command.profile,
      slug,
      profileId,
    });

    profile.commit();

    return profile.version;
  }
}
