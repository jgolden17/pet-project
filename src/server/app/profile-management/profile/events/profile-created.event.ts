import { Logger } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ID } from 'src/shared/core.types';
import { IProfile } from 'src/shared/profile.types';
import { ProfileRepository } from '../profile.repository';

export class ProfileCreated {
  constructor(public readonly id: ID, public readonly profile: IProfile) {}
}

@EventsHandler(ProfileCreated)
export class ProfileCreatedHandler implements IEventHandler<ProfileCreated> {
  private readonly logger = new Logger(ProfileCreatedHandler.name);

  constructor(private readonly repository: ProfileRepository) {}

  private async creatProfileProjection(event) {
    const { id, profile } = event;

    await this.repository.persist({
      id,
      ...profile,
    });
  }

  async handle(event: ProfileCreated) {
    await Promise.all([
      this.creatProfileProjection(event),
    ]);
  }
}
