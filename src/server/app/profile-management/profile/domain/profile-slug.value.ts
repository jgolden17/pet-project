import * as slug from 'slug';
import { InvariantViolation } from "src/server/lib/exceptions/invariant-violation.exception";

export class ProfileSlug {
  public readonly value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): ProfileSlug {
    if (!value) {
      throw new InvariantViolation('profileId value is required');
    }

    return new ProfileSlug(slug(value));
  }
}
