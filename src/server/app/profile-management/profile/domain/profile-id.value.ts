import { InvariantViolation } from "src/server/lib/exceptions/invariant-violation.exception";

export class ProfileId {
  public readonly value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string) {
    if (!value) {
      throw new InvariantViolation('profileId value is required');
    }

    return new ProfileId(value);
  }
}
