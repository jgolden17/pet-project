import { AggregateRoot } from 'src/server/app/lib/aggregate-root';
import { InvariantViolation } from 'src/server/lib/exceptions/invariant-violation.exception';
import { ID } from 'src/shared/core.types';
import { SpeciesEnum } from 'src/shared/profile.types';
import { ProfileCreated } from '../events/profile-created.event';
import { ProfileId } from './profile-id.value';
import { ProfileSlug } from './profile-slug.value';

interface ProfileProps {
  name: string;
  species: SpeciesEnum;
  slug: ProfileSlug;
  profileId: ProfileId;
}

export class Profile extends AggregateRoot {
  public readonly id: ID;
  public name: string;
  public species: SpeciesEnum;

  private _slug: ProfileSlug;
  private _profileId: ProfileId;

  constructor(id: ID) {
    super();
    this.id = id;
  }

  get profileId() {
    return this._profileId.value;
  }

  get slug() {
    return this._slug.value;
  }

  private set profileId(value: string) {
    this._profileId = ProfileId.create(value);
  }

  private set slug(value: string) {
    this._slug = ProfileSlug.create(value);
  }

  create(profile: Partial<ProfileProps>) {
    if (!profile.name) {
      throw new InvariantViolation('name is required');
    }

    if (profile.name.length <= 3) {
      throw new InvariantViolation('name should be greater than 3 character');
    }

    if (!profile.species) {
      throw new InvariantViolation('species is required');
    }

    this.apply(new ProfileCreated(this.id, {
      slug: profile.slug.value,
      profileId: profile.profileId.value,
      name: profile.name,
      species: profile.species,
    }));
  }

  onProfileCreated(event: ProfileCreated) {
    const { profileId, name, species, slug } = event.profile;

    this.name = name;
    this.species = species;
    this.profileId = profileId;
    this.slug = slug;
  }
}
