import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppSagas } from './app.saga';
import { AppService } from './app.service';
import { CommunicationsModule } from './communications/communications.module';
import { DatabaseModule } from './database/database.module';
import { ProfileModule } from './profile-management/profile/profile.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/pet-project'),
    DatabaseModule,
    ProfileModule,
    CommunicationsModule,
  ],
  controllers: [AppController],
  providers: [AppService, AppSagas],
})
export class AppModule {}
