export enum SpeciesEnum {
  dog = 'dog',
  cat = 'cat',
  human = 'human',
}

export interface IProfile {
  profileId: string;
  slug: string;
  name: string;
  species: SpeciesEnum;
}

export interface IProfileInput {
  profileId: string;
  slug: string;
  name: string;
  species: SpeciesEnum;
}

export interface ProfileDTO extends IProfile {}
